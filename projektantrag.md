# Projektantrag

Auszubildende:

- Philipp Scharfenberg
- Mattis Bollhorst
- Nils Kampmeier

## Projektbezeichnung

Aufbau und Entwicklung eines selbstgebauten, selbstbewässernden Blumentopfs mithilfe von Sensordaten

## Projektbeschreibung

### Projektumfeld

Das Umfeld ist die beispielhafte Projektumsetzung im Rahmen des Faches EVP am Berufskolleg Lübbecke während unserer Ausbildung zum Fachinformatiker. Status jetzt haben wir diverse Blumentöpfe, die von Hand gepflegt (d.h. gewässert, gedüngt, ...) werden müssen.

### Projektdefinition

Das zu realisierende Projekt befasst sich mit der automatisierten Bewässerung von Pflanzen. Hierzu wird ein Blumentopf mit Sensoren bestückt und die Daten ausgelesen. Die Daten werden protokolliert, anschließend wird die Pflanze automatisiert, basierend auf den Sensordaten, gewässert.

### Motivation

Weil Menschen Dinge vergessen, besteht das Risiko, dass Topfpflanzen z.B. vertrocknen. Durch permanentes Monitoring und automatisierte Reaktion auf die überwachten Werte wird zumindest das Absterben durch unregelmäßgige Pflege ausgeschlossen.

### Zielsetzung

Herstellung eines Blumentopfes, der automatisiert dafür sorgt, dass die Pflanze überlebt. Wir implementieren zuerst eine Automatisierung für die Bewässerung. Falls danach noch Zeit ist, kann das Projekt um weitere Sensoren, z.B. für Temperatur oder Düngemittelkonzentration, erweitert werden.

### Abgrenzungen

Für die Vernetzung des Blumentopfes werden off-the-shelf-Komponenten verwendet, wir planen eine Evaluierung zwischen Raspberry, einem vorhandenen Intel-NUC und einem ESP2866 sowie handelsüblichen Sensoren und bestehenden Softwarebibliotheken. Zur Visualisierung der Daten wird OpenHub oder andere ähnliche bestehende Software genutzt.

## Projektphasen

### Analyse

- Durchführung einer Ist-Analyse: 0,5 Stunden
- Erstellung eines Soll-Konzeptes: 0,5 Stunden
- Ermittlung von Use-Cases: 0,5 Stunden
- Evaluierung der Hardware (NUC vs. Raspberry vs ESP): 2 Stunden
- Erstellung der Kosten-Nutzen-Analyse: 1 Stunden

### Entwurf

- Erstellung eines Zeitplanes: 1 Stunde
- Erstellung der Use-Case-Diagramme: 1 Stunde
- Entwurf der Architektur: 1 Stunde

### Implementierung

- Erstellung der Benutzeroberfläche: 2 Stunden
- Realisierung der Verbindung zur Datenbank: 1 Stunde
- Realisierung der Verbindung zum Backend: 1 Stunde
- Erstellung der Hardware (d.h. 3D-Druck des Blumentopfes, Einbau der Sensoren, usw.):
  - Design der Druckvorlage: 3,5 Stunden
  - Druck des Blumentopfes: 2,5 Stunden
  - Einbau der Sensoren: 1,5 Stunden
  - Einbau des Minicomputers: 1,5 Stunden

### Testen

- Anwendung testen: 1,5 Stunden

### Dokumentation

- Erstellung der Projektdokumentation: 10 Stunden

### Projektphasen mit Zeitplänen in Stunden

- Analyse: 4,5 Stunden
- Entwurf: 3 Stunden
- Implementierung: 13 Stunden
- Testen: 1,5 Stunden
- Dokumentation: 10 Stunden
- Puffer: 1 Stunden
- Summe: 33 Stunden

## Zielgruppe der Präsentation

Zielgruppe sind Mitschüler und Lehrer die (Zimmer-)Pflanzen besitzen, aber das Risiko minimieren wollen, dass die Pflanze mangels Pflege eingeht.

## Geplante Präsentationsmittel

Hilfsmittel für die Präsentation sind ein Laptop und ein Beamer.
